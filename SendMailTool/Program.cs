﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using log4net;
using log4net.Appender;
using log4net.Repository;
using SendMailTool.Log;

namespace SendMailTool
{
    class Program
    {
        public enum RESULT
        {
            SUCCESS = 0,
            WARNING = 4,
            ERROR = 9,
        }

        //ログ出力変数
        private static string strIniPath = Path.Combine(Directory.GetCurrentDirectory(), @"Config\SendMail.ini"); //iniファイルパス
        //エラーコード値
        private const int RESULT_NORMAL = 0;//正常
        private const int RESULT_WARN = 4;  //警告
        private const int RESULT_ERR = 9;   //エラー

        public static TapFileAppender errorLog { get; set; }

        class IniFileHandler
        {
            [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringA")]
            public static extern int GetPrivateProfileString(string lpAppName
                                                    , string lpKeyName
                                                    , string lpDefault
                                                    , StringBuilder lpReturnedString
                                                    , int nSize
                                                    , string lpFileName);

            [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringA")]
            public static extern int WritePrivateProfileString(string lpAppName
                                        , string lpKeyName
                                        , string lpString
                                        , string lpFileName);
        }

        /// <summary>
        /// メール送信バッチ
        /// 第一引数：TYPE（処理成功・失敗）　第二引数：処理件数
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            #region 変数まとめ
            string MAIL_SUBJECT = "";
            string MAIL_ADDRESS_TO = "";
            string MAIL_ADDRESS_FROM = "";
            string MAIL_USER_ID = "";
            string MAIL_PASSWORD = "";
            string MAIL_HOST = "";
            string MAIL_PORT = "";
            string SAVED_LOG_DATE_COUNT = "";
            string LOG_PATH = "";
            #endregion
            try
            {
                //1.初期処理
                #region iniファイル読み込み
                try
                {
                    //iniファイルがあるか確認
                    if (!File.Exists(strIniPath))
                    {
                        Logger.Info("設定ファイル(SendMail.ini)がありません ");
                        //return RESULT_WARN;
                        return;
                    }
                    string strErrMsg = "";
                    MAIL_SUBJECT = args[0];
                    MAIL_ADDRESS_FROM = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_ADDRESS_FROM", "default", ref strErrMsg);
                    MAIL_ADDRESS_TO = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_ADDRESS_TO", "default", ref strErrMsg);
                    MAIL_USER_ID = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_USER_ID", "default", ref strErrMsg);
                    MAIL_PASSWORD = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_PASSWORD", "default", ref strErrMsg);
                    MAIL_HOST = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_HOST", "default", ref strErrMsg);
                    MAIL_PORT = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "MAIL_PORT", "default", ref strErrMsg);
                    SAVED_LOG_DATE_COUNT = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "SAVED_LOG_DATE_COUNT", "default", ref strErrMsg);
                    LOG_PATH = Program.getINIProfieString(strIniPath, "MAIL_SETTING", "LOG_PATH", "default", ref strErrMsg);

                }
                catch (Exception ex)
                {
                    //エラー発生ならば異常終了
                    Logger.Error("設定ファイル読み込み処理にてエラーが発生しました。 \r\n"
                                + "【エラー内容】" + ex.ToString(), ex);
                    throw ex;
                }
                #endregion

                #region log4net設定
                try
                {
                    #region log4netの出力先を変更
                    log4net.Repository.Hierarchy.Hierarchy infoRepo = (log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository();
                    TapFileAppender infoAppender = (TapFileAppender)infoRepo.Root.GetAppender("InfoLogDailyAppender");
                    log4net.Repository.Hierarchy.Hierarchy errorRepo = (log4net.Repository.Hierarchy.Hierarchy)log4net.LogManager.GetRepository();
                    TapFileAppender errorAppender = (TapFileAppender)errorRepo.Root.GetAppender("ErrorLogDailyAppender");

                    errorLog = errorAppender;

                    string infoLogPath = string.Format(@"{0}\[ConcurBridge]MailLog_info_", LOG_PATH);
                    string errorLogPath = string.Format(@"{0}\[ConcurBridge]MailLog_error_", LOG_PATH);
                    foreach (ILoggerRepository repository in LogManager.GetAllRepositories())
                    {
                        foreach (IAppender appender in repository.GetAppenders())
                        {
                            if (appender.Name.Equals("InfoLogDailyAppender"))
                            {
                                FileAppender fileAppender = appender as FileAppender;
                                if (fileAppender != null)
                                {
                                    string file = fileAppender.File;
                                    if (!string.IsNullOrEmpty(file))
                                    {
                                        fileAppender.File = infoLogPath;
                                        fileAppender.ActivateOptions();

                                        if (File.Exists(file))
                                        {
                                            File.Delete(file);
                                        }
                                    }
                                }
                            }
                            if (appender.Name.Equals("ErrorLogDailyAppender"))
                            {
                                FileAppender fileAppender = appender as FileAppender;
                                if (fileAppender != null)
                                {
                                    string file = fileAppender.File;
                                    if (!string.IsNullOrEmpty(file))
                                    {
                                        fileAppender.File = errorLogPath;
                                        fileAppender.ActivateOptions();

                                        if (File.Exists(file))
                                        {
                                            File.Delete(file);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion


                    #region 過去ログファイル削除
                    if (SAVED_LOG_DATE_COUNT != "" && int.TryParse(SAVED_LOG_DATE_COUNT, out var dateCount))
                    {
                        var dt = DateTime.Now.AddDays(-dateCount - 1);
                        var deleteLogDate = new DateTime(dt.Year, dt.Month, dt.Day);
                        // ログファイル抽出
                        var logLst = Directory.GetFiles(LOG_PATH, "[ConcurBridge]MailLog_*", SearchOption.TopDirectoryOnly).ToList();

                        foreach (var logFilg in logLst)
                        {
                            var timeStp = GetDateTime(logFilg.Substring(logFilg.LastIndexOf("_") + 1, 8));

                            if (timeStp <= deleteLogDate)
                            {
                                File.Delete(logFilg);
                            }
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {

                    //エラー発生ならば異常終了
                    Logger.Error("log4net設定にてエラーが発生しました。 \r\n"
                                + "【エラー内容】" + ex.ToString(), ex);
                    throw ex;
                }
                #endregion

                #region アラートメール送信処理
                Logger.Info("＝＝＝メール送信バッチ開始＝＝＝");
                Logger.Info(string.Format("起動パラメータ：{0}", string.Join(" ", args.ToList())));

                MailMessage mail = new MailMessage();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                try
                {

                    List<string> MailAddressList = new List<string>();

                    List<string> mailToLst = MAIL_ADDRESS_TO.Split(',').ToList();

                    // 宛先
                    mailToLst.ForEach(x => mail.To.Add(x));

                    //送信元アドレス
                    mail.From = new MailAddress(MAIL_ADDRESS_FROM);

                    //件名
                    mail.Subject = MAIL_SUBJECT;

                    //本文
                    mail.Body = args[1].Replace("\\r\\n", "\r\n");

                    //SmtpClientオブジェクトを作成する
                    smtp = new System.Net.Mail.SmtpClient();
                    //サーバ設定
                    smtp.Host = MAIL_HOST;
                    smtp.Port = int.Parse(MAIL_PORT);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // 本番環境
                    smtp.EnableSsl = false;
                    // テスト環境
                    //smtp.EnableSsl = true;

                    //ユーザー名とパスワードを設定する
                    smtp.Credentials = new System.Net.NetworkCredential(MAIL_USER_ID, MAIL_PASSWORD);

                    Logger.Info(string.Format("件名={0}", MAIL_SUBJECT));
                    Logger.Info(string.Format("本文={0}", args[1].Replace("\\r\\n", "\r\n")));

                    Logger.Info(string.Format("MAIL_ADDRESS_TO={0}", MAIL_ADDRESS_TO));
                    Logger.Info(string.Format("MAIL_ADDRESS_FROM={0}", MAIL_ADDRESS_FROM));
                    Logger.Info(string.Format("MAIL_USER_ID={0}", MAIL_USER_ID));
                    Logger.Info("MAIL_PASSWORD=*****");
                    Logger.Info(string.Format("MAIL_HOST={0}", MAIL_HOST));
                    Logger.Info(string.Format("MAIL_PORT={0}", MAIL_PORT));
                    Logger.Info(string.Format("SAVED_LOG_DATE_COUNT={0}", SAVED_LOG_DATE_COUNT));
                    Logger.Info(string.Format("LOG_PATH={0}", LOG_PATH));



                    //メールを送信する
                    smtp.Send(mail);
                    Logger.Info("メール送信完了");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    //解放
                    mail?.Dispose();
                    smtp?.Dispose();
                }
                Logger.Info("RESULT：" + RESULT.SUCCESS);
                Logger.Info("＝＝＝メール送信バッチ終了＝＝＝");
                #endregion
            }
            catch (Exception ex)
            {
                //エラー発生ならば異常終了
                Logger.Info("エラーログファイルを確認してください。");
                Logger.Info("RESULT：" + RESULT.ERROR);
                Logger.Error(string.Format("RESULT：{0} メール送信失敗", RESULT.ERROR), ex);
                return;
            }
            finally
            {
            }
        }
        #region INIファイル 設定内容取得
        /// <summary>
        ///　INIファイル 設定内容取得（共通化）
        /// </summary>
        /// <param name="strIniPath">iniファイルのpath</param>
        /// <param name="sMode">セクション</param>
        /// <param name="sPGID">プログラムID</param>
        /// <param name="sDef">デフォルト設定値</param>
        /// <param name="sErrMsg">エラーメッセージ</param>
        /// <param name="ErrWrite">
        /// エラー出力の有無
        /// デフォルトはtrue
        /// </param>
        /// <returns>取得した設定内容</returns>
        private static string getINIProfieString(string strIniPath, string sMode, string sPGID, string sDef, ref string sErrMsg, bool ErrWrite = true)
        {

            string strINITxt = "";
            StringBuilder sb = new StringBuilder(1024);

            try
            {

                IniFileHandler.GetPrivateProfileString(sMode, sPGID, "default", sb, sb.Capacity, strIniPath);
                if (sb.ToString().Equals("default"))
                {
                    if (ErrWrite)
                    {
                        sErrMsg += sMode + "セクションの" + sPGID + "が読めません。\r\n";
                    }
                    strINITxt = sDef;
                }
                else
                {
                    strINITxt = sb.ToString();
                }

                return strINITxt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        private static DateTime GetDateTime(string date)
        {
            if (date == "")
            {
                throw new Exception("日付が入力されていません。");
            }
            DateTime ret = new DateTime();
            if (DateTime.TryParse(date, out var parseDate))
            {
                ret = parseDate;
            }
            else
            {
                ret = DateTime.ParseExact(date, "yyyyMMdd", null);
            }
            return ret;
        }

    }
}
