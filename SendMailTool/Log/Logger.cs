﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Repository.Hierarchy;
using log4net.Util;
using System;
using System.IO;
using System.Reflection;

namespace SendMailTool.Log
{
    public static class Logger
    {
        static ILog logger = LogManager.GetLogger(Assembly.GetExecutingAssembly().FullName);


        public static void Debug(string message)
        {
            logger.Debug(message);

        }

        public static void Info(string message)
        {
            logger.Info(message);
        }

        public static void Warn(string message)
        {
            logger.Warn(message);
        }

        public static void Error(string message, Exception exception)
        {
            logger.Error(message, exception);
        }

        public static void Fatal(string message)
        {
            logger.Fatal(message);
        }
    }
    public class TapFileAppender : RollingFileAppender
    {
        private TapQuietTextWriter _tapWriter = null;

        // FileAppenderが書き込みに使う QuietWriter プロパティを
        // セットする箇所を上書き
        override protected void SetQWForFiles(TextWriter writer)
        {
            _tapWriter = new TapQuietTextWriter(writer, ErrorHandler);
            base.SetQWForFiles(_tapWriter);
        }

        public String Text
        {
            get { return _tapWriter.Text; }
        }
    }

    public class TapQuietTextWriter : QuietTextWriter
    {
        private StringWriter _tapWriter;

        public TapQuietTextWriter(TextWriter writer, IErrorHandler errorHandler)
            : base(writer, errorHandler)
        {
            _tapWriter = new StringWriter();
        }

        public override void Write(char value)
        {
            base.Write(value);
            _tapWriter.Write(value);
        }

        public override void Write(char[] buffer, int index, int count)
        {
            base.Write(buffer, index, count);
            _tapWriter.Write(buffer, index, count);
        }

        override public void Write(string value)
        {
            base.Write(value);
            _tapWriter.Write(value);
        }

        public String Text
        {
            get { return _tapWriter.ToString(); }
        }
    }
}
